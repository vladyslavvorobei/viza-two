import Slick from 'slick-carousel';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';



// -- -- -- BEGIN scripts -- -- --

 // -- -- -- SCROLL scripts BEGIN -- -- --

$(document).scroll(function() {
  // -- -- -- BEGIN SCROLL HEADER-NAV -- -- --
  if($(this).scrollTop() > 70) {
    $('.header-nav__wrapper').addClass('scroll');
    $('.header-main').addClass('scroll');
  }
  else {
    $('.header-nav__wrapper').removeClass('scroll');
    $('.header-main').removeClass('scroll');
    $('.header-nav__wrapper').removeClass('active');
    $('.header-main__wrapper').removeClass('active');
    $('.header-nav__more-btn_burger').removeClass('active');
  }
  // -- -- -- END SCROLL HEADER-NAV -- -- --
});

// -- -- -- SCROLL scripts END -- -- --



// -- -- -- DOCUMENT-READY scripts BEGIN -- -- --

$(document).ready(function() {
  // -- -- -- ANCHORM-MENU script BEGIN -- -- --
  $('.nav-main a').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });
  // -- -- -- ANCHORM-MENU script END -- -- --
  // -- -- -- DROP-MENU-CLICK scripts BEGIN -- -- --
  $('.header-nav__more-btn').click(function () {
    $('.header-nav__wrapper').toggleClass('active');
    $('.header-nav__more-btn_burger').toggleClass('active');
    $('.header-main__wrapper').toggleClass('active');
    $(document).mouseup(function(e) {
      let div = $('.header-nav__wrapper');
      if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('.header-nav__wrapper').removeClass('active');
        $('.header-nav__more-btn_burger').removeClass('active');
      }
    });
  });
  // -- -- -- DROP-MENU-CLICK scripts END -- -- --


  // -- -- -- ADD SLICK-SLIDER for REVIEWS-BLOCK  BEGIN script -- -- --

  $('.reviews__slider').slick({
    centerMode: true,
    centerPadding: '500px',
    slidesToShow: 1,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1700,
        settings: {
          centerPadding: '400px',
        }
      },
      {
        breakpoint: 1500,
        settings: {
          centerPadding: '300px',
        }
      },
      {
        breakpoint: 1300,
        settings: {
          centerPadding: '200px',
        }
      },
      {
        breakpoint: 1200,
        settings: {
          centerPadding: '150px',
        }
      },
      {
        breakpoint: 991,
        settings: {
          centerMode: false,
        }
      },
    ]
  });
  // -- -- -- ADD SLICK-SLIDER for REVIEWS-BLOCK  END script -- -- --

  // -- -- -- MODAL-POPUP -- -- -- BEGIN
  $('.modal, .modal-callback, .modal-review').popup({
    transition: 'all 0.3s',
    outline: true,
    focusdelay: 400,
    vertical: 'top',
    closebutton: true
  });
  // -- -- -- MODAL-POPUP -- -- -- END
  // -- -- -- jQuery Mask + jquery VALIDATION script BEGIN -- -- --
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phone_number, element) {
    return this.optional(element) || phone_number.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        country: 'required',
        agree: 'required',
        formName: 'required',
        reviews: 'required',
        tel: 'required',
      },
      messages: {
        name: 'Введите Ваше имя',
        tel: 'Введите Ваш телефон',
        agree: 'Нужно соглашение на обработку данных',
        reviews: 'Оставьте свой отзыв'
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function(res) {
        $('.modal, .modal-callback, .modal-review').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
        console.log(res);
      }
    });

  };
  // -- -- -- jQuery Mask + jquery VALIDATION script END -- -- --
});

// -- -- -- DOCUMENT-READY scripts END -- -- --

// -- -- -- END scripts -- -- --
